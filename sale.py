# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from sql.aggregate import Max
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['SaleLine']


class SaleLine:
    __name__ = 'sale.line'
    __metaclass__ = PoolMeta

    last_prices = fields.Function(
        fields.Many2Many('sale.line', None, None, 'Last prices',
                        domain=[('product', '=', Eval('product'))],
                        depends=['product']),
        'get_last_prices')
    sale_date = fields.Function(
        fields.Date('Sale date'), 'get_sale_date')

    @fields.depends('product', '_parent_sale.id',
                    '_parent_sale.party', '_parent_sale.sale_date')
    def on_change_with_last_prices(self):
        return self.get_last_prices([self])[self.id]

    @classmethod
    def get_last_prices(cls, records, name=None):
        pool = Pool()
        Sale = pool.get('sale.sale')
        sale = Sale.__table__()
        sale_line = cls.__table__()
        cursor = Transaction().connection.cursor()

        res = {r.id: [] for r in records}
        for record in records:
            cursor.execute(*sale_line.join(
                sale, condition=(sale.id == sale_line.sale)
                ).select(Max(sale_line.id),
                     where=((sale_line.type == 'line') &
                            (sale.id != record.sale.id) &
                            (sale_line.product == record.product.id) &
                            (sale.party == record.sale.party.id) &
                            (sale.state != 'cancelled') &
                            (sale.sale_date <= record.sale.sale_date)),
                     order_by=(sale_line.product, sale.id),
                     group_by=(sale_line.product, sale.id),
                     limit=3)
            )
            res[record.id] = [row[0] for row in cursor.fetchall()] or []
        return res

    @classmethod
    def get_sale_date(cls, records, name=None):
        return {r.id: r.sale.sale_date for r in records}
